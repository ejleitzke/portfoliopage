﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portfolio_Page.Models;

namespace Portfolio_Page.ViewModels
{
    public class RandomMovieViewModel
    {
        public Movie Movie { get; set; }
        public List<Customer> Customers { get; set; }
    }
}